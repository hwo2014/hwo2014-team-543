#include <errno.h>
#include <netdb.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdbool.h>

#include "cJSON.h"

#ifndef game__h
#define game__h

#define MAX_NAME_LEN 25
#define MAX_COLOR_LEN 15

//typedef game
typedef struct _game_t game_t;

//car struct with positions, dimensions, id
typedef struct _car_t{
	
	//static attributes
	char 	*name;
	char	*color;
	double 	length;
	double 	width;
	double 	guide_flag_position;
	
	//changing attributes
	double 	angle;
	double 	in_piece_distance;
	int 	piece_index;
	int 	start_lane;
	int 	end_lane;
	int 	lap;
	double	throttle;
} car_t;

//track piece struct with radius/length, angle, switch
typedef struct _piece_t{
	double	length;
	double	radius;
	double	angle;
	bool	switchable;
} piece_t;

//lane struct
typedef struct _lane_t{
	double distance_from_center;
	int index;
} lane_t;

//game struct contains an array of car structs, an array of
//track piece structs, and some metadata about the race
struct _game_t{
	car_t 	*cars;
	piece_t *pieces;
	lane_t 	*lanes;

	car_t 	me;

    int		num_cars;
    int 	num_pieces;
    int 	num_lanes;
    int 	max_time;
    int 	laps;

    bool	quick_race;

    char*	track_name;

};

//init the game struct
game_t *game_init(cJSON *init_msg);

//destroy the game struct
void game_destroy(game_t *game);

//set the throttle for the car
void set_throttle(game_t *game, float throttle);

#endif