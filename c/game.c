#include "game.h"

game_t *game_init(cJSON *json){
	int n_cars;
	//various JSON Objects
	cJSON *data = cJSON_GetObjectItem(json, "data");
	cJSON *race_info = cJSON_GetObjectItem(data, "race");
	cJSON *track = cJSON_GetObjectItem(race_info, "track");
	cJSON *pieces = cJSON_GetObjectItem(track, "pieces");
	cJSON *lanes = cJSON_GetObjectItem(track, "lanes");
	cJSON *cars = cJSON_GetObjectItem(race_info,"cars");
	cJSON *race = cJSON_GetObjectItem(race_info,"raceSession");
	cJSON *piece;
	cJSON *length;

	//malloc new game
	game_t *new_game = (game_t *) malloc(sizeof(game_t));

	//print data
	//printf("Server Data:\n%s\n",cJSON_Print(data));

	//get the starting track info
	new_game->num_pieces = cJSON_GetArraySize(pieces);
	new_game->pieces = (piece_t *) malloc(new_game->num_pieces * sizeof(piece_t));
	//printf("There are %d pieces.\n",new_game->num_pieces);
	
	//add actual pieces to struct, checking for NULL and setting to zero where appropriate
	for(int i=0; i<(new_game->num_pieces); i++){
		//printf("Beginning of loop...\n");

		piece = cJSON_GetArrayItem(pieces,i);
		length = cJSON_GetObjectItem(piece,"length");
		
		//piece length
		if(length != NULL){
			(new_game->pieces)[i].length = length->valuedouble;
		}
		else{
			(new_game->pieces)[i].length = 0.0;
		}
		//printf("Got length...\n");

		//piece radius
		if(cJSON_GetObjectItem(piece,"radius") != NULL){
			(new_game->pieces)[i].radius = cJSON_GetObjectItem(piece,"radius")->valuedouble;
		}
		else{
			(new_game->pieces)[i].radius = 0.0;
		}
		//printf("Got radius...\n");

		//piece angle
		if(cJSON_GetObjectItem(piece,"angle") != NULL){
			(new_game->pieces)[i].angle = cJSON_GetObjectItem(piece,"angle")->valuedouble;
		}
		else{
			(new_game->pieces)[i].angle = 0.0;
		}
		//printf("Got angle...\n");

		//piece switchable
		if(cJSON_GetObjectItem(piece,"switch") != NULL){
			(new_game->pieces)[i].switchable = true;
		}
		else{
			(new_game->pieces)[i].switchable = false;
		}
		//printf("Got switchable...\n");

		printf("Piece %d is %f long, has a radius of %f, has an angle of %f, and switchable is %d.\n", i,(new_game->pieces)[i].length,(new_game->pieces)[i].radius,(new_game->pieces)[i].angle,(new_game->pieces)[i].switchable);
		
	}

	//get the starting cars info
    
    //get num cars and print
    n_cars = cJSON_GetArraySize(cars);
    new_game->num_cars = n_cars;
    new_game->cars = (car_t *) malloc(new_game->num_cars * sizeof(car_t));
    //printf("There are %d cars.\n", n_cars);
    
    //print the info for each car
    for(int i=0;i<n_cars;i++){
    	cJSON *car = cJSON_GetArrayItem(cars,i);
    	cJSON *id = cJSON_GetObjectItem(car,"id");
    	cJSON *dimensions = cJSON_GetObjectItem(car,"dimensions");
    	//printf("Car #%d:\n%s\n",i,cJSON_Print(car));
    	//printf("Car %s is %s.\n",cJSON_GetObjectItem(id,"name")->valuestring,cJSON_GetObjectItem(id,"color")->valuestring);
    	
    	//copy over the values
    	new_game->cars[i].name = (char *) malloc((strlen(cJSON_GetObjectItem(id,"name")->valuestring))* sizeof(char));
    	new_game->cars[i].color = (char *) malloc((strlen(cJSON_GetObjectItem(id,"color")->valuestring))* sizeof(char));

    	//printf("Malloc'ed new strings...\n");

    	strcpy(new_game->cars[i].name, cJSON_GetObjectItem(id,"name")->valuestring);
    	strcpy(new_game->cars[i].color, cJSON_GetObjectItem(id,"color")->valuestring);

    	//printf("Copied new strings...\n");

    	new_game->cars[i].length = cJSON_GetObjectItem(dimensions,"length")->valuedouble;
    	new_game->cars[i].guide_flag_position = cJSON_GetObjectItem(dimensions,"guideFlagPosition")->valuedouble;
    	new_game->cars[i].width = cJSON_GetObjectItem(dimensions,"width")->valuedouble;
    	
    	//printf("Copied other attributes...\n");
    	if(!strcmp(new_game->cars[i].name,"LeadFoot")){
    		new_game->me = new_game->cars[i];
    		printf("My name is %s, and I am %s.\n",new_game->me.name,new_game->me.color);
    	}

    }

    //get the lane info
    new_game->num_lanes = cJSON_GetArraySize(lanes);
    new_game->lanes = (lane_t *) malloc(new_game->num_lanes * sizeof(lane_t));

    for(int i = 0; i<new_game->num_lanes; i++){
    	cJSON *lane = cJSON_GetArrayItem(lanes,i);
    	new_game->lanes[i].index = cJSON_GetObjectItem(lane,"index")->valueint;
    	new_game->lanes[i].distance_from_center = cJSON_GetObjectItem(lane,"distanceFromCenter")->valuedouble;
    }

    //get the race info
    new_game->laps = cJSON_GetObjectItem(race,"laps")->valueint;
    new_game->quick_race = cJSON_GetObjectItem(race,"quickRace")->type;
    new_game->max_time = cJSON_GetObjectItem(race,"maxLapTimeMs")->valueint;

    new_game->track_name = (char *) malloc(strlen(cJSON_GetObjectItem(track,"name")->valuestring) * sizeof(char));
    strcpy(new_game->track_name,cJSON_GetObjectItem(track,"name")->valuestring);

	return (new_game);
}

void game_destroy(game_t *game){
	free(game->pieces);
	free(game->cars);
	free(game->lanes);
	free(game->track_name);
	free(game);
}

void set_throttle(game_t *game, float throttle){
	game->me.throttle = throttle;
}